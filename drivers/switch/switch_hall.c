/*
 * Hall sensor driver
 *
 * Author: Ben Hsu <ben.hsu@acer.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/switch.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/workqueue.h>
#include <linux/input.h>
#include <linux/wakelock.h>
#if defined(CONFIG_FB)
#include <linux/notifier.h>
#include <linux/fb.h>
#elif defined(CONFIG_HAS_EARLYSUSPEND)
#include <linux/earlysuspend.h>
#endif

struct hall_platform_data {
	const char *name;
	int gpio;
	u32 irq_flags;
	int debounce_ms;
	int swcode;
};

struct hall_sensor_data {
	struct device *dev;
	struct switch_dev sdev;
	struct input_dev *input_dev;
	struct delayed_work state_delay_work;
#if defined(CONFIG_FB)
	struct notifier_block fb_notif;
	int fb_status;
#elif defined(CONFIG_HAS_EARLYSUSPEND)
	struct early_suspend early_suspend;
#endif
	int irq_gpio;
	u32 irq_gpio_flags;
	unsigned int irq;
	bool device_sleep;
	bool enable;
	spinlock_t lock;
	struct wake_lock irq_wake_lock;
	int debounce_ms;
	int swcode;
};

#define DRIVER_NAME				"hall-sensor"
#define DEVICE_NAME				"S-5712"
#define SENSOR_NAME				"hall"

#define HALL_BUF_LEN			16
#define HALL_WAKEUP_ENABLE		1
#define HALL_WAKEUP_DISABLE		0
#define HALL_DEF_DEBOUNCE_MS	200
#define HALL_DEF_CODE			SW_LID

#define HALL_TAG				"[Hall]"
#define hall_err(fmt, args...)	printk(KERN_ERR HALL_TAG fmt, ##args)
#define hall_dbg(fmt, args...)	printk(HALL_TAG fmt, ##args)


struct hall_sensor_data *hsdev;
static int hall_sensor_suspend(struct device *dev);
static int hall_sensor_resume(struct device *dev);
static void hall_sensor_enable_irq(struct hall_sensor_data *hsdata, bool enable);
static void hall_sensor_send_event(struct hall_sensor_data *hsdata, int swcode,
			int state);

static ssize_t hall_enable_show(struct device *dev,
						struct device_attribute *attr, char *buf)
{
	struct switch_dev *sdev = dev_get_drvdata(dev);
	struct hall_sensor_data *hsdata = container_of(sdev, struct hall_sensor_data,
													sdev);
	int count = 0;

	if (!hsdata) {
		hall_dbg("Invalid data\n");
		goto exit;
	}

	count = sprintf(buf, "%s\n",
				(hsdata->enable) ? "enable" : "disable");

exit:
	return count;
}

static ssize_t hall_enable_store(struct device *dev,
						struct device_attribute *attr, const char *buf,
						size_t size)
{
	struct switch_dev *sdev = dev_get_drvdata(dev);
	struct hall_sensor_data *hsdata = container_of(sdev, struct hall_sensor_data,
													sdev);
	int err = 0;
	int enable = 0;

	if (!hsdata) {
		hall_dbg("Invalid data\n");
		goto exit;
	}

	err = sscanf(buf, "%d", &enable);
	if (err < 0) {
		hall_dbg("Enable hall sensor failed: %d\n", err);
		goto exit;
	}

	enable = !!enable;

	if (!device_may_wakeup(hsdata->dev))
		goto exit;

	if ((enable) && (!hsdata->enable)) {
		hall_sensor_enable_irq(hsdata, true);
	} else if (!(enable) && (hsdata->enable)) {
		hall_sensor_enable_irq(hsdata, false);
	}

	hsdata->enable = enable;

	if (enable) {
		wake_lock_timeout(&hsdata->irq_wake_lock, hsdata->debounce_ms * 2);
		schedule_delayed_work(&hsdata->state_delay_work,
				msecs_to_jiffies(hsdata->debounce_ms));
	} else {
		/* relesase hall sensor event when disable */
		if (switch_get_state(sdev))
			hall_sensor_send_event(hsdata, hsdata->swcode, 0);
	}

#if defined(DEBUG)
	hall_dbg("Set hall sensor status : %d\n", (int)hsdata->enable);
#endif

exit:
	return size;
}

static DEVICE_ATTR(enable, S_IRUGO | S_IWUSR, hall_enable_show,
					hall_enable_store);

static struct attribute *hall_sensor_attrs[] = {
	&dev_attr_enable.attr,
	NULL,
};

static const struct attribute_group hall_sensor_attr_group = {
	.attrs = hall_sensor_attrs,
};

#if defined(CONFIG_FB)
static int hall_fb_notifier_callback(struct notifier_block *self,
						unsigned long event, void *data)
{
	struct fb_event *evdata = data;
	int *blank;
	int err = 0;
	struct hall_sensor_data *hsdata =
		container_of(self, struct hall_sensor_data, fb_notif);

	if (evdata && evdata->data && event == FB_EVENT_BLANK && hsdata) {
		blank = evdata->data;

		if (*blank == hsdata->fb_status)
			goto exit;

		if (*blank == FB_BLANK_UNBLANK)
			err = hall_sensor_resume(hsdata->dev);
		else if (*blank == FB_BLANK_POWERDOWN)
			err = hall_sensor_suspend(hsdata->dev);

		if (err == 0)
			hsdata->fb_status = *blank;
	}

exit:
	return err;
}
#elif defined(CONFIG_HAS_EARLYSUSPEND)
static void hall_sensor_early_suspend(struct early_suspend *es)
{
	struct hall_sensor_data *hsdata = container_of(es, struct hall_sensor_data,
													early_suspend);

	hall_sensor_suspend(hsdata->dev);
}

static void hall_sensor_late_resume(struct early_suspend *es)
{
	struct hall_sensor_data *hsdata = container_of(es, struct hall_sensor_data,
													early_suspend);
	hall_sensor_resume(hsdata->dev);
}
#endif

#if defined(CONFIG_PM)
static int hall_sensor_suspend(struct device *dev)
{
	struct hall_sensor_data *hsdata = dev_get_drvdata(dev);
	int err = 0;

	if (hsdata == NULL) {
		hall_dbg("Invalid data\n");
		err = -EINVAL;
		goto exit;
	}

	spin_lock(&hsdata->lock);
	hsdata->device_sleep = true;
	spin_unlock(&hsdata->lock);

exit:
	return err;
}

static int hall_sensor_resume(struct device *dev)
{
	struct hall_sensor_data *hsdata = dev_get_drvdata(dev);
	int err = 0;

	if (hsdata == NULL) {
		hall_dbg("Invalid data\n");
		err = -EINVAL;
		goto exit;
	}

	spin_lock(&hsdata->lock);
	hsdata->device_sleep = false;
	spin_unlock(&hsdata->lock);

exit:
	return err;
}

static const struct dev_pm_ops hall_sensor_pm_ops = {
#if !defined(CONFIG_FB) && !defined(CONFIG_HAS_EARLYSUSPEND)
	.suspend	= hall_sensor_suspend,
	.resume		= hall_sensor_resume,
#endif
};
#endif

static void hall_sensor_send_event(struct hall_sensor_data *hsdata, int swcode,
			int state)
{
	input_report_switch(hsdata->input_dev, swcode, state);
	input_sync(hsdata->input_dev);
}

static ssize_t switch_print_state(struct switch_dev *sdev, char *buf)
{
	return sprintf(buf, "%s\n", (switch_get_state(sdev) ? "1" : "0"));
}

static ssize_t switch_print_name(struct switch_dev *sdev, char *buf)
{
	return sprintf(buf, "%s\n", DEVICE_NAME);
}

static void hall_sensor_work(struct work_struct *work)
{
	struct hall_sensor_data *hsdata = container_of(work,
										struct hall_sensor_data,
										state_delay_work.work);
	int gpio_status = 0;
	int switch_state = 0;
	unsigned long flags;

	if (hsdata == NULL) {
		hall_dbg("Invalid data\n");
		goto err_out;
	}

	if (gpio_is_valid(hsdata->irq_gpio)) {
		gpio_status = gpio_get_value(hsdata->irq_gpio);
#if defined(DEBUG)
		hall_dbg("Get gpio status : %d\n", gpio_status);
#endif
	}

	/* hall sensor gpio state
	 * gpio value    switch state
	 *     0              1
	 *     1              0
	 */
	switch_state = !gpio_status;
	switch_set_state(&hsdata->sdev, switch_state);
	hall_dbg("Hall sensor state change : %d\n", switch_state);

	spin_lock_irqsave(&hsdata->lock, flags);
	/* Hall sensor suspend/wakeup device debug message */
	if ((hsdata->swcode == SW_LID) && (gpio_status == hsdata->device_sleep)) {
		if (hsdata->device_sleep)
			hall_dbg("Hall sensor wakeup device\n");
		else
			hall_dbg("Hall sensor suspend device\n");
	}
	hall_sensor_send_event(hsdata, hsdata->swcode, switch_state);

	spin_unlock_irqrestore(&hsdata->lock, flags);
err_out:
	wake_unlock(&hsdata->irq_wake_lock);
}

static irqreturn_t hall_sensor_irq_handler(int irq, void *dev_id)
{
	struct hall_sensor_data *hsdata = (struct hall_sensor_data *)dev_id;

	if (hsdata == NULL) {
		hall_dbg("Invalid data\n");
		goto exit;
	}

	wake_lock_timeout(&hsdata->irq_wake_lock, hsdata->debounce_ms * 2);
	schedule_delayed_work(&hsdata->state_delay_work,
				msecs_to_jiffies(hsdata->debounce_ms));

exit:
	return IRQ_HANDLED;
}

static void hall_sensor_enable_irq(struct hall_sensor_data *hsdata, bool enable)
{
	if (enable) {
		enable_irq(hsdata->irq);
		enable_irq_wake(hsdata->irq);
	} else {
		disable_irq_wake(hsdata->irq);
		disable_irq(hsdata->irq);
	}
}

static int hall_sensor_parse_dt(struct device *dev,
					struct hall_platform_data *pdata)
{
	struct device_node *np = dev->of_node;
	u32 value;

	pdata->gpio = of_get_named_gpio_flags(np, "Seiko,irq-gpio",
							0, &value);
	if (gpio_is_valid(pdata->gpio))
		pdata->irq_flags = value;
	else
		return -EINVAL;

	pdata->name = of_get_property(np, "Seiko,input-name", NULL);

	if (of_property_read_u32(np, "Seiko,code", &value) == 0)
		pdata->swcode = value;
	else
		pdata->swcode = HALL_DEF_CODE;

	if (of_property_read_u32(np, "Seiko,debounce-ms", &value) == 0)
		pdata->debounce_ms = value;
	else
		pdata->debounce_ms = HALL_DEF_DEBOUNCE_MS;

	return 0;
}

static int __devinit hall_sensor_probe(struct platform_device *pdev)
{
	struct hall_platform_data *pdata = NULL;
	struct hall_sensor_data *hsdata = NULL;
	struct input_dev *input_dev;
	int err = 0;

	if (pdev->dev.of_node) {
		pdata = devm_kzalloc(&pdev->dev, sizeof(struct hall_platform_data),
					GFP_KERNEL);
		if (!pdata) {
			hall_err("Failed to allocate memory");
			return -ENOMEM;
		}

		err = hall_sensor_parse_dt(&pdev->dev, pdata);
		if (err)
			goto err_free_mem;
	} else {
		pdata = pdev->dev.platform_data;
	}

	if (!pdata) {
		err = -EINVAL;
		goto err_free_mem;
	}

	hsdata = devm_kzalloc(&pdev->dev, sizeof(struct hall_sensor_data),
			GFP_KERNEL);
	if (!hsdata) {
		hall_err("Failed to allocate memory");
		goto err_free_mem;
	}

	hsdata->dev = &pdev->dev;
	hsdata->irq_gpio = pdata->gpio;
	hsdata->irq = gpio_to_irq(hsdata->irq_gpio);
	hsdata->irq_gpio_flags = pdata->irq_flags;
	hsdata->debounce_ms = pdata->debounce_ms;
	hsdata->swcode = pdata->swcode;

	input_dev = input_allocate_device();
	if (input_dev == NULL) {
		hall_err("Failed to allocate input device\n");
		goto err_free_mem;
	}

	input_dev->name = pdata->name;
	input_dev->dev.parent = &pdev->dev;
	input_dev->id.bustype = BUS_HOST;

	__set_bit(EV_SW, input_dev->evbit);
	__set_bit(hsdata->swcode, input_dev->swbit);
	spin_lock_init(&hsdata->lock);
	wake_lock_init(&hsdata->irq_wake_lock, WAKE_LOCK_SUSPEND, pdata->name);

	hsdata->input_dev = input_dev;

	err = input_register_device(input_dev);
	if (err) {
		hall_err("Failed to register input device\n");
		goto err_free_input_dev;
	}

	/* GPIO irq */
	err = gpio_request(hsdata->irq_gpio, "hall-irq-gpio");
	if (err) {
		dev_err(&pdev->dev, "[Hall] Failed to request irq gpio\n");
		goto err_unregister_input;
	}

	err = gpio_direction_input(hsdata->irq_gpio);
	if (err) {
		dev_err(&pdev->dev, "[Hall] Unable to set direction for irq gpio %d\n",
				hsdata->irq_gpio);
		goto err_free_irq_gpio;
	}

	err = request_threaded_irq(hsdata->irq, NULL, hall_sensor_irq_handler,
								hsdata->irq_gpio_flags, DRIVER_NAME,
								hsdata);
	if (err) {
		dev_err(&pdev->dev, "[Hall] Failed to request irq %d\n", hsdata->irq);
		goto err_free_irq_gpio;
	}

	hsdata->sdev.print_state = switch_print_state;
	hsdata->sdev.name = SENSOR_NAME;
	hsdata->sdev.print_name = switch_print_name;
	err = switch_dev_register(&hsdata->sdev);
	if (err) {
		hall_err("Register switch device failed\n");
		goto err_free_irq;
	}

	err = sysfs_create_group(&hsdata->sdev.dev->kobj, &hall_sensor_attr_group);
	if (err) {
		hall_err("Failed to create sysfs group:%d\n", err);
		goto err_unregister_switch;
	}

#if defined(CONFIG_FB)
	hsdata->fb_notif.notifier_call = hall_fb_notifier_callback;
	err = fb_register_client(&hsdata->fb_notif);
	if (err) {
		dev_err(&pdev->dev, "[Hall] Failed to register fb_notifier:%d\n", err);
		goto err_unregister_switch;
	}
#elif defined(CONFIG_HAS_EARLYSUSPEND)
	if (hsdata->swcode == SW_LID) {
		hsdata->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + 1;
		hsdata->early_suspend.suspend = hall_sensor_early_suspend;
		hsdata->early_suspend.resume = hall_sensor_late_resume;
		register_early_suspend(&hsdata->early_suspend);
	}
#endif
	dev_set_drvdata(&pdev->dev, hsdata);
	INIT_DELAYED_WORK(&hsdata->state_delay_work, hall_sensor_work);

	/* default enable hall sensor */
	hsdata->enable = true;
	device_init_wakeup(&pdev->dev, HALL_WAKEUP_ENABLE);


	hsdev = hsdata;
	hall_sensor_enable_irq(hsdata, true);

	if ((pdev->dev.of_node) && (pdata != NULL))
		devm_kfree(&pdev->dev, (void *)pdata);

	return 0;

err_unregister_switch:
	switch_dev_unregister(&hsdata->sdev);
err_free_irq:
	free_irq(hsdata->irq, hsdata);
err_free_irq_gpio:
	if (gpio_is_valid(hsdata->irq_gpio))
		gpio_free(hsdata->irq_gpio);
err_unregister_input:
	input_unregister_device(hsdata->input_dev);
err_free_input_dev:
	if (input_dev)
		input_free_device(input_dev);
err_free_mem:
	if (hsdata != NULL)
		devm_kfree(&pdev->dev, (void *)hsdata);
	if ((pdev->dev.of_node) && (pdata != NULL))
		devm_kfree(&pdev->dev, (void *)pdata);
	return err;
}

static int __devexit hall_sensor_remove(struct platform_device *pdev)
{
	struct hall_sensor_data *hsdata = platform_get_drvdata(pdev);

	cancel_delayed_work_sync(&hsdata->state_delay_work);
#if defined(CONFIG_FB)
	fb_unregister_client(&hsdata->fb_notif);
#elif defined(CONFIG_HAS_EARLYSUSPEND)
	unregister_early_suspend(&hsdata->early_suspend);
#endif
	switch_dev_unregister(&hsdata->sdev);
	free_irq(hsdata->irq, hsdata);
	if (gpio_is_valid(hsdata->irq_gpio))
		gpio_free(hsdata->irq_gpio);
	input_unregister_device(hsdata->input_dev);
	input_free_device(hsdata->input_dev);
	disable_irq_wake(hsdata->irq);
	device_init_wakeup(&pdev->dev, HALL_WAKEUP_DISABLE);
	devm_kfree(&pdev->dev, (void *)hsdata);

	return 0;
}

static struct of_device_id hall_sensor_of_match[] = {
	{ .compatible = "Seiko,hall-sensor",},
	{ },
};
MODULE_DEVICE_TABLE(of, hall_sensor_of_match);


static struct platform_driver hall_sensor_device_driver = {
	.probe		= hall_sensor_probe,
	.remove		= __devexit_p(hall_sensor_remove),
	.driver		= {
		.name	= "hall-sensor",
		.owner	= THIS_MODULE,
		.of_match_table = hall_sensor_of_match,
#if defined(CONFIG_PM)
		.pm		= &hall_sensor_pm_ops,
#endif
	}
};

static int __init hall_senor_init(void)
{
	return platform_driver_register(&hall_sensor_device_driver);
}

static void __exit hall_sensor_exit(void)
{
	platform_driver_unregister(&hall_sensor_device_driver);
}

late_initcall(hall_senor_init);
module_exit(hall_sensor_exit);

MODULE_AUTHOR("Ben Hsu <ben.hsu@acer.com>");
MODULE_DESCRIPTION("Seiko S-5712ACDL1-I4T1U hall sensor driver");
MODULE_LICENSE("GPL");
