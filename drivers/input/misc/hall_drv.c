#include <linux/device.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/timer.h>
#include <linux/input.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/sysfs.h>
#include <linux/workqueue.h>

#define SENSOR_GPIO 52
#define EINT_HALL   52
#define KEY_HALL    (KEY_POWER)   //116

#define RAPID_GPIO  107
#define EINT_RAPID  107
#define KEY_RAPID   (KEY_F2)  //60
#define DELAY       10
//static struct timer_list hs_timer;
struct gpio_reso {
	unsigned long gpio;
	char *name;
	struct mutex value_mutex;
	int irq;
};
static struct gpio_reso gpio_info[] = {
	[0]={
		.gpio = SENSOR_GPIO, 
		.name = "Hsensor",
		.irq  = EINT_HALL,
	},
	[1]={
		.gpio = RAPID_GPIO,
		.name = "Rapid",
		.irq  = EINT_RAPID
	}
}; 

struct input_dev *dev = NULL;

static int read_gpio_status(unsigned long gpio) {
	if (gpio_direction_input(gpio) < 0) {
		printk("---lever gpio direction input failed!---\n\r");
		return -1;
	}
	return gpio_get_value(gpio);
}

/*
	read gpio data
*/
static ssize_t gpio_status_show(struct device *dev, 
		struct device_attribute *attr, char *buf) {
	static int dat = 0;

	mutex_lock(&gpio_info[0].value_mutex);
	dat = read_gpio_status(gpio_info[0].gpio);
	mutex_unlock(&gpio_info[0].value_mutex);

	if (dat == -1) {
		printk("---lever read gpio failed---\n\r");
		return -1;
	}
	return sprintf(buf, "%d\n", dat);
}
static ssize_t rapid_status_show(struct device *dev, 
		struct device_attribute *attr, char *buf) {
	static int dat = 0;

	mutex_lock(&gpio_info[1].value_mutex);
	dat = read_gpio_status(gpio_info[1].gpio);
	mutex_unlock(&gpio_info[1].value_mutex);

	if (dat == -1) {
		printk("---lever read gpio failed---\n\r");
		return -1;
	}
	return sprintf(buf, "%d\n", dat);
}

//write data do nothing
static ssize_t gpio_status_store(struct device *dev,
		struct device_attribute *attr, const char *buf,
			size_t count) {
	return count;
}
static ssize_t rapid_status_store(struct device *dev,
		struct device_attribute *attr, const char *buf,
			size_t count) {
	return count;
}

//work struct 
static struct workqueue_struct *hall_wq = NULL;
static struct workqueue_struct *rapid_wq = NULL;
static void hall_do_work(struct work_struct *work) {
	if (read_gpio_status(gpio_info[0].gpio) == 0) {
		msleep(DELAY);
		if (read_gpio_status(gpio_info[0].gpio) == 0) {
			input_report_key(dev,KEY_HALL, 1);
			input_report_key(dev,KEY_HALL, 0);
			input_sync(dev);
		}
	}
/*	if (read_gpio_status(gpio_info[0].gpio) == 1) {
		msleep(DELAY);
		if (read_gpio_status(gpio_info[0].gpio) == 1) {
			input_report_key(dev,KEY_HALL, 1);
			input_report_key(dev,KEY_HALL, 0);
			input_sync(dev);
		}
	} */
	enable_irq(gpio_info[0].irq);
}

static void rapid_do_work(struct work_struct *work) {
	if (read_gpio_status(gpio_info[1].gpio) == 0) {
		msleep(DELAY);
		if (read_gpio_status(gpio_info[1].gpio) == 0) {
			input_report_key(dev,KEY_RAPID, 1);
			input_sync(dev);
		}
	}
	if (read_gpio_status(gpio_info[1].gpio) == 1) {
		msleep(DELAY);
		if (read_gpio_status(gpio_info[1].gpio) == 1) {
			input_report_key(dev,KEY_RAPID, 0);
			input_sync(dev);
		}
	}
	enable_irq(gpio_info[1].irq);
}

static DECLARE_WORK(hall_work, hall_do_work);
static DECLARE_WORK(rapid_work, rapid_do_work);

//create device attribute
static DEVICE_ATTR(HallStatus,0644,gpio_status_show,gpio_status_store);
static DEVICE_ATTR(Rapid,0644,rapid_status_show,rapid_status_store);

static struct attribute *gpio_attributes[] = {
	&dev_attr_HallStatus.attr,
	&dev_attr_Rapid.attr,
	NULL
};

static struct attribute_group gpio_attribute_group = {
	.attrs = gpio_attributes
};
/*
static void hs_gpio_func(unsigned long data) {
	static int dat = 0;
	int data_gpio = *(int *)data;
	dat = read_gpio_status(data_gpio);
	if (dat == -1) {
		printk("---read gpio failed---\n\r");
	}
	mod_timer(&hs_timer, jiffies + 2 * HZ);
}
*/

//interrupt handler
static irqreturn_t gpio_handler(int irq, void *data) {
/*	
	input_report_key(dev,KEY_HALL, 1);
	input_report_key(dev,KEY_HALL, 0);
	input_sync(dev);
*/
	disable_irq_nosync(gpio_info[0].irq);
	queue_work(hall_wq, &hall_work);
	return IRQ_HANDLED;
}
static irqreturn_t rapid_handler(int irq, void *data) {
	disable_irq_nosync(gpio_info[1].irq);
	queue_work(rapid_wq, &rapid_work);
	return IRQ_HANDLED;
}

static int gpio_init(void) {
	int ret, i;
	dev = input_allocate_device();
	dev->name = "hall_sensor";
	__set_bit(EV_KEY, dev->evbit);
	set_bit(KEY_POWER, dev->keybit);
	set_bit(KEY_F2, dev->keybit);
	ret = input_register_device(dev);
	device_init_wakeup(&(dev->dev),1);
	if (ret < 0) {
		input_free_device(dev);
		printk("lever input register failed!\n\r");
		return -1;
	}
	ret = sysfs_create_group(&dev->dev.kobj , &gpio_attribute_group);
	if (ret < 0) {
		input_unregister_device(dev);
		input_free_device(dev);
		printk("lever sysfs create failed!\n\r");
		return -1;
	}
	hall_wq = create_workqueue("hall wq");
	rapid_wq = create_workqueue("rapid wq");
	if (hall_wq == NULL) {
		printk("lever create work queue failed!\n\r");
		destroy_workqueue(hall_wq);
		return -1;
	}
/*
	init_timer(&hs_timer);
	hs_timer.expires = jiffies + 2 * HZ;
	hs_timer.function = hs_gpio_func;
	hs_timer.data = (unsigned long)&gpio_info.gpio;
	add_timer(&hs_timer); 
*/
	for (i = 0; i < ARRAY_SIZE(gpio_info); i++) {
		mutex_init(&gpio_info[i].value_mutex);
		if (gpio_request(gpio_info[i].gpio, gpio_info[i].name) < 0 ) {
			printk("---lever gpio request failed!---\n\r");
			return -1;
		}
		gpio_info[i].irq = gpio_to_irq(gpio_info[i].irq);
		
	}
	ret = request_irq(gpio_info[0].irq,gpio_handler,IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING,
					gpio_info[0].name,&gpio_info[0]);
	
	if ( ret < 0) {
		printk("---lever hall irq request failed %d!---\n\r",ret);
		return -1;
	}
	ret = request_irq(gpio_info[1].irq,rapid_handler,IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING,
						gpio_info[1].name,&gpio_info[1]);
	
	if ( ret < 0) {
		printk("---lever rapid irq request failed %d!---\n\r",ret);
		return -1;
	}
	return 0;
}

static void gpio_exit(void) {
	int i = 0;
//	del_timer(&hs_timer);
	input_unregister_device(dev);
	input_free_device(dev);
	destroy_workqueue(hall_wq);
	for (i = 0; i < ARRAY_SIZE(gpio_info); i++) {
		free_irq(gpio_info[i].irq, &gpio_info[i]);
		gpio_free(gpio_info[i].gpio);
	}
}

module_init(gpio_init);
module_exit(gpio_exit);
MODULE_LICENSE("GPL");
//MODULE_LICENSE("GPL");
